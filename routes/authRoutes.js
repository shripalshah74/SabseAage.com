const passport = require('passport');
  // original passport npm module

module.exports = app => {
  app.get('/auth/google',
   passport.authenticate('google', {
    scope: ['profile', 'email']
    })
  );

  app.get(
    '/auth/google/callback',
    passport.authenticate('google'),
  (req,res) => {
      res.redirect('/surveys');
  }
);

  app.get('/api/logout', (req,res) => {
    req.logout();
      res.redirect('/');

  });

  // app.get('/getUserName', (req,res) => {
  //   // res.send(req.);
  // })
  app.get('/api/current_user', (req, res) => {

    res.send(req.user);

    if(req.user == null){
      console.log('User not logged in');
    }
  });
};
