const sendgrid = require('sendgrid');// send grid module installed from npm
const helper = sendgrid.mail;// helper object we create from the property sendgrid.mamil  . This helps us create the mailer
const keys = require('../config/keys');

class Mailer extends helper.Mail{
  //  helper.Mail property is an object that takes lot of configuratin and spits out a mailer. We take that Mail class and add additional customization to it

  // Properties that we set up.
  // first pargument just has to be an object , content is just the html string
  constructor({subject, recipients}, content) {
    super();// call all the extended constructor

    this.sgApi = sendgrid(keys.sendGridKey);
    this.from_email = new helper.Email('no-reply@sabseaage.com');
    this.subject = subject;
    this.body = new helper.Content('text/html', content);
    this.recipients = this.formatAddresses(recipients);

    this.addContent(this.body);
    this.addClickTracking();
    this.addRecipients();

  }

  formatAddresses(recipients) {
    return recipients.map(({ email })=>{
      return new helper.Email(email);
    });
  }

  addClickTracking() {
    const trackingSettings = new helper.TrackingSettings();
    const clickTracking = new helper.ClickTracking(true,true);

    trackingSettings.setClickTracking(clickTracking);
    this.addTrackingSettings(trackingSettings);
  }

  addRecipients() {
    const personalize = new helper.Personalization();
    this.recipients.forEach(recipient => {
      personalize.addTo(recipient);
    });
    this.addPersonalization(personalize);
  }

  async send(){
      const request = this.sgApi.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: this.toJSON()
      });

      const response = await this.sgApi.API(request);
      return response;
  }
}


module.exports = Mailer;
