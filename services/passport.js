const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const mongoose = require('mongoose');
const keys = require('../config/keys');

// const UserName = mongoose.model('userName');
const User = mongoose.model('users');
// forming cookie
passport.serializeUser((user, done ) => {
  done(null, user.id)
});
// forming record from cookie
passport.deserializeUser((id, done) => {
  User.findById(id)
  .then(user => {
    done(null, user);
  });
});
/*
  Install a helper library cookie session to manage cookies in our application
*/
// don't use require statements for mongoose model classes.
passport.use(new GoogleStrategy({
  clientID: keys.googleClientID,
  clientSecret: keys.googleClientSecret,
  callbackURL:'/auth/google/callback',
  proxy:true
},
async (accessToken, refreshToken, profile, done) =>{
      console.log(profile.name.givenName , 'passport.js');
      // const uName= new UserName ({googleName: profile.name.givenName});
      const existingUser = await User.findOne({googleId: profile.id})
          if(existingUser){
          // we already have a recordwith a given profile id
          console.log(existingUser);
          console.log('Record exists');
          console.log(profile.emails);
          return done(null, existingUser);
        }
        // we don't have a user record with ID, make a new record
        const user = await new User({googleId: profile.id, userName: profile.name.givenName}).save();
        console.log('Record created');
        done(null, user);

    }
  )
);
