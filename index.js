const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const bodyParser = require('body-parser');
const keys = require('./config/keys');
require('./models/User');
require('./models/Survey');
require('./services/passport');

// instruct mongoose to connect the copy of mongodb on mlab.com
mongoose.connect(keys.mongoURI);
const app =express();// this generates new application in the single Node project we may have many express applications
app.use(bodyParser.json());
app.use(
  cookieSession({
    maxAge: 30 * 24 * 60* 60 * 1000,
    keys: [keys.cookieKey]
  })
);

app.use(passport.initialize());
app.use(passport.session());

require('./routes/authRoutes')(app);
require('./routes/billingRoutes.js')(app);
require('./routes/surveyRoutes.js')(app);

if(process.env.NODE_ENV === 'production') {
  // making sure that express will serve up production assets like main.js file , or main.css file
  app.use(express.static('client/build'));
  // Expressn will serve up the index.html file if it doesn't recognize the route
    // require the module path
    const path = require('path');
    app.get('*', (req,res) => {
      res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });

}

// the argument google is an internal identifier for GoogleStrategy
// scope specifies to google servers what access we want to have inside of this users profile



// public token.. okay if anyone has this id.. we can share this with public.
// Private Token - We don't want anyone to see this!
// inside the GoogleStrategy constructor, we are going to pass configuration parameters to let google know about the information on how to authenticate the users.
// creating route handler and associate it with a route

const PORT = process.env.PORT || 5000; // hereoku will assign thte port number runtime using env variables
app.listen(PORT); // localhost:5000 you'll see hi: there  you can use an addon on chrome to make that look beautiful
