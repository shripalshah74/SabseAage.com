const mongoose = require('mongoose');
//const Schema = mongoose.Schema;
const{Schema} = mongoose; //destructuring Schema const Schema= mongoose.Schema.. Schema is a mongoose property

// Mongoose want's to know ahea of time about what all property it has

const userSchema = new Schema({
  googleId: String,
  credits: { type:  Number, default: 5},
  userName: String
});

// User will look like the above Schema
// const userName = new Schema({
//   googleName: String
// });
// mongoose.model('userName', userName);
mongoose.model('users', userSchema);
// mongoose will create the collection if it doesn't exist.. It won't create the already created ones.
