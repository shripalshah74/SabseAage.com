import axios from 'axios';
import {FETCH_USER, FETCH_SURVEYS} from './types';


export const fetchUser = () => async dispatch =>{
    const res = await axios.get('/api/current_user');

    dispatch({type:FETCH_USER, payload: res.data});

};
// Action Creator handle token returned from Stripe after the user has made the payemnt.

export const handleToken = token => async dispatch => {
  const res = await axios.post('/api/stripe', token);
  // backend returns the current user model
  dispatch({type: FETCH_USER, payload:res.data});

};


export const submitSurvey = (values, history) => async dispatch =>{
  // this is our action creator
  const res = await axios.post('/api/surveys', values);
  console.log(res);

  history.push('/surveys');
  dispatch({type: FETCH_USER, payload: res.data});
};


export const fetchSurveys = () => async dispatch => {
  const res = await axios.get('/api/surveys');

  dispatch({type: FETCH_SURVEYS, payload: res.data});
};
