import React from 'react';
const FeedbackForm = () => {


  function getUrlVars() {
      var vars = {};
      var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
          vars[key] = value;
      });

      return vars;
  }
  function getUrlParam(parameter, defaultvalue){
    var urlparameter = defaultvalue;
    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = getUrlVars()[parameter];
        }
    return urlparameter;
}

    const id = getUrlParam('id', false);
    if(!id){
      return(<div style={{textAlign: 'center', color:'red'}}>
        <h1>Thank you for the input</h1>
        <p>Expand your business by subscribing to our services!</p>
        <ul >
          <li>Register your company with us and reach out to your customers</li>
          <li>Send Feedback Campaigns in no time!</li>
          <li>Get Tailored information from the campaigns you create</li>
          <li>Provide new features to your customers easily and hassle free</li>
        </ul>
        </div>
      );
    }
return (
  <div>
  <p>
  {id}
  </p>
  </div>

);

};

export default FeedbackForm;
