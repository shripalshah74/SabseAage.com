import React from 'react';
import { Link } from 'react-router-dom';

const Landing = () => {
  return (
    <div className="container">
  <div className="section" >
  <h3 style ={{textAlign: 'center', color: 'green'}}>Sabse Aage</h3>


  <div className="section no-pad-bot" id="index-banner" >
    <div className="container">
      <br/><br/>
      <div className="row center">
        <h5 className="header col s12 light">A Modern Responsive Survey Application</h5>
      </div>
      <div className="row center">
        <a href="/surveys" id="download-button" className="btn-large waves-effect waves-light orange">Get Started</a>
      </div>
      <br/><br/>
    </div>
  </div>




    <div className="row">
      <div className="col s12 m4">
        <div className="icon-block">
          <h2 className="center red-text"><i className="material-icons">flash_on</i></h2>
          <h5 className="center">Speeds Up Development</h5>

          <p className="light" style = {{textAlign:'center'}}>Collect feedback from your customers using Sabse Aage's Platform. Get tailored stats based on feedbacks received.</p>
        </div>
      </div>

      <div className="col s12 m4">
        <div className="icon-block">
          <h2 className="center light-blue-text"><i className="material-icons">group</i></h2>
          <h5 className="center">User Experience Focused</h5>

          <p className="light" style = {{textAlign:'center'}}>Our aim is to provide features with minimum complexity. Our model meets user's needs by increasing responsiveness.</p>
        </div>
      </div>

      <div className="col s12 m4">
        <div className="icon-block">
          <h2 className="center yellow-text"><i className="material-icons">settings</i></h2>
          <h5 className="center">Easy To Work With</h5>

          <p className="light" style = {{textAlign:'center'}}>We are working on dashboard User Inerface with variety of features. We are also always open to feedback and can answer any questions a user may have about our platform.</p>
        </div>
      </div>
    </div>
    <div className="fixed-action-btn" style= {{heigth: '20%', padding: '0% 0% 5%'}}>
  <Link to="/surveys" className="btn-floating pulse z-depth-5 btn-large #00897b teal darken-1">
    <i className="large material-icons" >menu</i>
  </Link></div>
  </div>
  <br/><br/>
</div>

    //
    // <div className = "container"style={{ textAlign: 'center', color: 'black' }}>
    //   <h1>Sabse Aage</h1>
    //   <p style={{ color: 'black', textAlign: 'left' }}>
    //     Collect feedback from your customers using Sabse Aages platform
    //   </p>
    //   <div className="fixed-action-btn" style= {{heigth: '20%', padding: '0% 0% 5%'}}>
    // <Link to="/surveys" className="btn-floating pulse z-depth-5 btn-large #00897b teal darken-1">
    //   <i className="large material-icons" >menu</i>
    // </Link></div>
    // </div>
  );
};

export default Landing;
