import React from 'react';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import SurveyList from './surveys/SurveyList';



const Dashboard = ({auth}) => {

  if(auth){
  return (

      <div className= "container">
        <SurveyList/>
        <div className="fixed-action-btn" style= {{heigth: '20%', padding: '0% 0% 5%'}}>
      <Link to="/surveys/new" className="btn-floating pulse z-depth-5 btn-large #00897b teal darken-1">
        <i className="large material-icons" >add</i>
      </Link></div>
      </div>
      );

    }
    return (
      <div><h4 style={{textAlign: 'center'}}>Please Login first</h4></div>
    )

};

function mapStateToProps({auth}){
  return{auth};
}

export default connect(mapStateToProps) (Dashboard);
