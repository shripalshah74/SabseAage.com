// Survey new shows SurveyForm and SurveyFormReview
import React, {Component} from 'react';
import {reduxForm} from 'redux-form';
import SurveyForm from './SurveyForm';
import {Link} from 'react-router-dom';
import SurveyFormReview from './SurveyFormReview';

class SurveyNew extends Component {
// component level state
// constructor (props) {
//   super(props);
//
//   this.state = {new: true};
// }

state = { showFormReview: false};

renderContent() {
  if(this.state.showFormReview) {
    return (
      <SurveyFormReview
      onCancel= {() => this.setState({showFormReview: false})}
      />
    );
  }
  return <SurveyForm
    onSurveySubmit={() => this.setState({showFormReview: true})}
  />;
}

  render() {

    return (
      <div>
      

      {this.renderContent()}


        <div className="fixed-action-btn" style= {{heigth: '20%', padding: '0% 0% 5%'}}>
      <Link to="/surveys" className="btn-floating pulse z-depth-5 btn-large #00897b teal darken-1">
        <i className="large material-icons" >menu</i>
      </Link></div>
      </div>
    );



}
}


export default reduxForm({
  form: 'surveyForm'
// by default destroyOnUnmount is true
}) (SurveyNew);
