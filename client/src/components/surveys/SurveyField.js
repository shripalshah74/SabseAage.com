// SurveyField contains logic to render a single label and text input

import React from 'react';

export default ({input, label, meta,meta: {error, touched}}) => {

  return (
    <div>
    <label>
      {label}
    </label>
      <input {...input} style={{marginBottom: 5}}/>
      <div className="pink-text  darken-4" style={{marginBottom:'20px'}}> { touched && error}
      </div>
    </div>
  );
};
