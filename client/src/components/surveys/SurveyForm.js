// SurveyForm shows a form for a user to add input
import _ from 'lodash';
import React, {Component} from 'react';
import { reduxForm, Field } from 'redux-form';// helps us to communicate with the redux store
import SurveyField from './SurveyField';
import {Link} from 'react-router-dom';
import validateEmails from '../../utils/validateEmails';
import formFields from './formFields';

//find this in formFields.js
// const FIELDS = [
//   {label:'Survey Title', name: 'title', noValueError:'You must provide a title'},
//   {label:'Subject Line', name: 'subject', noValueError: 'You must provide a subject line'},
//   {label:'Email Body', name: 'body', noValueError: ' You must provide a body'},
//   {label:'Recipient List', name: 'emails', noValueError: 'You must provide at least one recipient'}
// ];

class SurveyForm extends Component {

  renderFields() {
    return _.map(formFields, ({ label, name }) => {
      return <Field key={name} component={SurveyField} type="text" label = {label} name= {name}/>
    })
  }


  render() {
    return (
      <div className="container" style={{width:'100%', padding: '5%'}}>
      <h4 style = {{textAlign: 'center', bottomMargin: '0px'}}> Enter the feedback contents: </h4>
      <label style={{color: 'red'}}>Recipients list should be comma(,) separated</label>
      <form onSubmit={this.props.handleSubmit(this.props.onSurveySubmit)}>
        {this.renderFields()}
        <Link to="/surveys" className="red z-depth-5 btn-large darken-1 left">Cancel
        <i className="material-icons right">clear_all</i></Link>
        <button className=" z-depth-5 btn-large #00897b teal darken-1 right " type="submit" >Next
        <i className="material-icons right">chevron_right</i>
        </button>

      </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  errors.recipients = validateEmails(values.recipients || '');
    _.each(formFields, ({name, noValueError}) => {
      if(!values[name]){
        errors[name] = noValueError;
      }
    });

  return errors;

}

export default reduxForm({
  validate,
  form: 'surveyForm',
  destroyOnUnmount: false
})(SurveyForm);
