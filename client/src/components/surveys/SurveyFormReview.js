// fUNCTIONAAL component Survey form review shows users their form input revuew
// how to access the information from previous component
import _ from 'lodash';
import React from 'react';
import {connect} from 'react-redux';
import formFields from './formFields';
import { withRouter } from 'react-router-dom';
import * as actions from '../../actions';


const SurveyFormReview = ({onCancel, formValues, submitSurvey, history }) => {
const reviewFields = _.map(formFields, ({name,label})=> {
  return (
    <div key={name}  style={{marginBottom: 15}}>
      <label  style={{marginBottom: 5}}>{label}</label>
      <div  style={{marginBottom: 5}}>
        {formValues[name]}
      </div>
    </div>
  );
});
  return(
      <div className = "container">
        <h5>Please confirm your entries</h5>
        {reviewFields}
        <button className="z-depth-5 btn-large #f9a825 yellow darken-3 left" onClick={onCancel}>
          Back
          <i className="material-icons right">chevron_left</i>
        </button>
        <button
        onClick = {() => submitSurvey(formValues, history)}
        className=" z-depth-5 btn-large #1b5e20 green darken-4 right " >Send Survey
        <i className="material-icons right">email</i>
        </button>

      </div>
  );
};

function mapStateToProps(state) {
  // console.log(state);
  return { formValues: state.form.surveyForm.values };
}

export default connect(mapStateToProps, actions)(withRouter(SurveyFormReview));
