import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className=" page-footer #1a237e indigo darken-4">
      <div className="row">
      <div className=" col s12 m4 footer-copyright " style={{textAlign: 'center'}}>© 2018 Sabse Aage </div>
      <div className="col s12 m4" style={{textAlign: 'center'}}>shripalshah74@hotmail.com</div>
      <div className="col s12 m4" style={{textAlign: 'right'}}> +1 971 770 8066 </div>
      </div>
        </footer>
    );
  }
}
export default Footer;
