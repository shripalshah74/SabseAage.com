
import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../actions';

import './style.css';
import Header from './Header';
import Footer from './Footer';
import Landing from './Landing';
import FeedbackForm from './FeedbackForm';
import Dashboard from './Dashboard';
import SurveyNew from './surveys/SurveyNew';


// const SurveyNew = () => <h2>SurveyNew ... Working On It</h2>
class App extends Component {
componentDidMount(){
  this.props.fetchUser();
}


  render(){
    return (
        <BrowserRouter>
          <div  style={{width:'100%'}}>

          <body>
            <Header/>
            <main>
            <Route exact path = "/" component = {Landing}/>
            <Route exact path = "/surveys" component = {Dashboard}/>
            <Route exact path = "/surveys/new" component = {SurveyNew}/>

            <Route exact path = "/feedbackform" component = {FeedbackForm}/>
            </main>
            <Footer/>
            </body>
          </div>
        </BrowserRouter>


    );
  }
}




export default connect(null, actions)(App);
