import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import Payments from './Payments';

class Header extends Component{
  renderContent(){
    switch (this.props.auth) {
      case null:
        return;
      case false:
        return (<li><a href="/auth/google">Login With Google</a></li>);
      default:
        return [
          // <li key="4" style={{margin:'0 10px'}}>Welcome {this.props.auth.userName}</li>,
          <li key="1"><Payments/> </li>,
          <li key="3" style={{margin:'0 10px'}}>Credits:{this.props.auth.credits}</li>,
          <li key="2"><a href="/api/logout">Logout</a></li>];
    }
  }
  render() {
      // console.log(this.props.auth);
      // console.log(this.props);
      return (<div>
        <nav  style={{height:'65px'}}>
          <div className="nav-wrapper #1a237e indigo darken-4 z-depth-5" >
            <Link to = "/"
            // to={this.props.auth?'/surveys':'/'}
            className="left brand-logo"
            style={{left:'5%'}}>SA
            </Link>
              <ul  className="right">
                {this.renderContent()}
              </ul>
          </div>
        </nav>

        </div>
      );
  }
}
function mapStateToProps({auth}){

  return{auth};
}
export default connect(mapStateToProps) (Header);
